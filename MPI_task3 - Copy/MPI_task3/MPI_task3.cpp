﻿// MPI_task2.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include "mpi.h"
#include <iostream>
#include <random>
#include <ctime>
#include <fstream>
#include "stdafx.h"
#include <string>


using namespace std;

void division(int* arr, int left, int right, int &t) {
	int x = arr[left];
	t = left;
	for (int i = left + 1; i < right; i++)
		if (arr[i] < x) {
			t++;
			//cout << arr[t] << "  " << arr[left] << ":" << t << ":" << left << " DIVIS  "<<x<<endl;
			swap(arr[i], arr[t]);
		}
	//cout << arr[t] << "  " << arr[left] << ":" << t << ":" << left << " DIVIS" << endl;
	swap(arr[t], arr[left]);
}
void qs(int* arr, int left, int right) {
	if (left < right) {
		int t = 0;
		division(arr, left, right,t);
		qs(arr, left, t);
		qs(arr, t + 1, right);
	}
}
void Print(int* A, int N, char message[]) {
	std::cout << message << endl;
	for (int i = 0; i < N; i++)
	std::cout << "A(" << i << ") = " << A[i] << std::endl;
	std::cout << std::endl;
	cout << "norm"<<endl;
}

int main(int argc, char* argv[])
{
	int rank, size;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status;
	int* arr, *part;
	int length, part_length, first = 0;
	int x = size - 1;;
	int midle = 0, left = 0, right;
	int pos = 0,count = 0, pos1 = 0,right1 = 0;

	double start_parallel, end_parallel, end_not_parallel, start_not_parallel;

	if (rank == 0) {
		cout <<"Proc NUM"<< size << endl;
		
		length = 5000000;
		int* copy = new int[length];
		arr = new int[length];
		part = arr;
		srand(MPI_Wtime());
		
		cout << "COPY ARR :"<< endl;
		for (int i = 0; i < length; i++) {
			arr[i] = rand()%1000;
			copy[i] = arr[i];
			//cout << copy[i] << endl;
		}
		
		start_not_parallel = MPI_Wtime();
		qs(copy, 0, length);
		end_not_parallel = MPI_Wtime();
		
		cout << "After qs S " << std::endl;
		/*for (int i = 0; i < length; i++)
			std::cout << copy[i] << std::endl;
		*/std::cout << "time: " << end_not_parallel - start_not_parallel << endl;
		delete[] copy;
		right = length;
		
		cout << "ARR array: " << std::endl;
		/*for (int i = 0; i < length; i++)
			std::cout << arr[i] << std::endl;
		*/cout << endl;
		int k = length * 0.001;
		cout << "ASDASDASD " << k<<endl;
		start_parallel = MPI_Wtime();
		while (x > 0 && left != right) {
			pos = count = 0;
			cout << "0X " << x <<  "midle: " << midle << "  right: " << right << " left:  " << left << endl;
			cout << "div rank  "<< rank << endl;
			division(arr, left, right, midle);
			//which part lower
			cout << "0X " << x << "midle: " << midle << "  right: " << right << " left:  " << left << endl;
			if (midle - left  < right - midle) {
				cout << "LLLL" << endl;
				//send left part
				pos = left;
				count = midle - left;
				left = midle + 1;
				pos1 = left;
				right1 = right;
			} else {
				cout << "RRRR" << endl;
				//send right part
				pos1 = pos;
				right1 = midle + 1; 
				pos = midle + 1;
				count = right - midle - 1;
				right = midle;
			}
			if (count == 0)
				count = 1;
			cout << "SEND MESS " << rank << "  " << pos << "   " << count << endl;
			/*for (int i = pos; i < pos + count; i++) {
				cout << arr[i] << endl;
			}
			*/cout << "SEND MESS2 " << rank << "  " << pos1 << "   " << right1 << endl;
			/*for (int i = pos1; i < right1; i++) {
				cout << arr[i] << endl;
			}*/
			if (count > k) {
				cout << "Send: " << pos + part << " " << count << " " << pos << " " << x << endl;
				MPI_Send(&pos, 1, MPI_INT, x, 0, MPI_COMM_WORLD);
				MPI_Send(&count, 1, MPI_INT, x, 0, MPI_COMM_WORLD);
				MPI_Send(part + pos, count, MPI_INT, x, 0, MPI_COMM_WORLD);
				cout << "End send" << endl;
				x--;
			}
			cout << "2X " << x <<  "midle:" << midle << "right" << right << " left:" << left << endl;
		}
	} else {
		cout << "Rank" << rank <<" wait" <<endl;
		MPI_Recv(&first, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&part_length, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		part = new int[part_length];
		cout << "receive: " << first << " ---- " << part_length << " R " << rank << endl;
		MPI_Recv(part, part_length, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		cout << "End receive: " << part << " ---- " << part_length << " R " << rank << endl;
		/*for (int i = 0; i < part_length; i++) {
			cout << i << " : " << part[i] << " :" << rank << endl;
		}*/
		right = part_length;
	}
	if (rank == 0) {
		left = pos1;
		right = right1;
	}

	qs(part, left, right);

	if (rank == 0) {
		x = size -1;
		cout << "RECEIVE START" << endl;
		while (x > 0) {
			int pos, count;
			cout << "RECEIVE START "<< x << endl;
			MPI_Recv(&pos, 1, MPI_INT, x, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&count, 1, MPI_INT, x, 0, MPI_COMM_WORLD, &status);
			cout << "RECEIVE: " << pos << " |=======|" << count << " R: " << rank << endl;
			MPI_Recv(part + pos, count, MPI_INT, x, 0, MPI_COMM_WORLD, &status);
			cout << "RECEIVE END" << endl;
			x--;
			/*for (int i = pos; i < length; i++) {
				cout << arr[i] << endl;
			}*/
		}
		end_parallel = MPI_Wtime();
	}
	if (rank != 0) {
		cout << "SEND M from: " << rank << endl;
		/*for (int i = first; i < part_length; i++) {
			cout << part[i] << endl;
		}
		*/
		cout << "SEND: " << part_length <<endl;
		MPI_Send(&first, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&part_length, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
		MPI_Send(part, part_length, MPI_INT, 0, 0, MPI_COMM_WORLD);
		cout << "SEND END" << endl;
		delete[] part;
	} else {
		cout << "RESULTS" << endl;
		//Print(arr, length, "FINAL ARR");
		cout << "Sorting time by " << size << ":      " << end_parallel - start_parallel << " sec" << endl;
		std::cout << "time: " << end_not_parallel - start_not_parallel << endl;
		delete[] arr;
	}
	MPI_Finalize();
	return 0;
}